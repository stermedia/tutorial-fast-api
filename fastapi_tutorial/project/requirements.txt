fastapi==0.68.0
uvicorn==0.14.0
SQLAlchemy==1.4.22
psycopg2-binary==2.9.1
pytest==6.2.4
requests==2.26.0