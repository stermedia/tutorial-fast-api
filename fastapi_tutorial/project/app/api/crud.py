from typing import List, Optional

from sqlalchemy.orm import Session

from app.api import models
from app.api.schemas import BlogPostCreate, BlogPostUpdate


def create_blog_post(db: Session, blog_post_body: BlogPostCreate) -> models.BlogPost:
    db_blog_post = models.BlogPost(**blog_post_body.dict())
    db.add(db_blog_post)
    db.commit()
    db.refresh(db_blog_post)

    return db_blog_post


def get_blog_post_by_id(db: Session, blog_post_id: int) -> models.BlogPost:
    db_blog_post = db.query(models.BlogPost).get(blog_post_id)
    return db_blog_post


def get_blog_posts(db: Session) -> List[models.BlogPost]:
    db_blog_post_list = db.query(models.BlogPost).all()
    return db_blog_post_list


def update_blog_post(db: Session, blog_post_id: int, blog_post_update_body: BlogPostUpdate) -> \
        Optional[models.BlogPost]:
    db_blog_post = db.query(models.BlogPost) \
        .filter(models.BlogPost.id == blog_post_id).first()
    if not db_blog_post:
        return None

    db_blog_post.title = blog_post_update_body.title
    db_blog_post.text = blog_post_update_body.text
    db_blog_post.is_visible = blog_post_update_body.is_visible
    db.commit()
    return db_blog_post


def delete_blog_post(db: Session, blog_post_id: int) -> bool:
    is_deleted = db.query(models.BlogPost).filter(models.BlogPost.id == blog_post_id).delete()
    db.commit()
    return is_deleted
