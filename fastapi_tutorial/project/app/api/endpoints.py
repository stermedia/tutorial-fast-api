from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app.api import crud
from app.api.schemas import BlogPostCreate, BlogPost, BlogPostUpdate
from app.database import get_db

router = APIRouter(prefix='/posts')


@router.post('/', response_model=BlogPost, status_code=status.HTTP_201_CREATED)
def create_post(blog_post_body: BlogPostCreate, db: Session = Depends(get_db)) -> BlogPost:
    blog_post = crud.create_blog_post(db, blog_post_body)

    if not blog_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Error creating blog post")
    return blog_post


@router.get('/{blog_post_id}', response_model=BlogPost, status_code=status.HTTP_200_OK)
def get_post_by_id(blog_post_id: int, db: Session = Depends(get_db)) -> BlogPost:
    blog_post = crud.get_blog_post_by_id(db, blog_post_id)

    if not blog_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Blog post with given id does not exist.")
    return blog_post


@router.get('/', response_model=List[BlogPost], status_code=status.HTTP_200_OK)
def get_posts_list(db: Session = Depends(get_db)) -> List[BlogPost]:
    blog_posts = crud.get_blog_posts(db)
    return blog_posts


@router.put('/{blog_post_id}', response_model=BlogPost, status_code=status.HTTP_200_OK)
def update_post(blog_post_id: int, blog_post_update_body: BlogPostUpdate, db: Session = Depends(get_db)) -> BlogPost:
    blog_post = crud.update_blog_post(db, blog_post_id, blog_post_update_body)

    if not blog_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Blog post has not been updated.")

    return blog_post


@router.delete('/{blog_post_id}', status_code=status.HTTP_200_OK)
def delete_post(blog_post_id: int, db: Session = Depends(get_db)) -> dict:
    is_deleted = crud.delete_blog_post(db, blog_post_id)

    if not is_deleted:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Blog post has not been deleted.")

    return {'detail': 'Blog post has been deleted.'}
