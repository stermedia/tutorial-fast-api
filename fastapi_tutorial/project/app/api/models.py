from sqlalchemy import Column, Integer, String, Boolean, DateTime, func

from app.database import Base


class BlogPost(Base):
    __tablename__ = "blog_posts"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    text = Column(String)
    date_added = Column(DateTime, default=func.now())
    date_updated = Column(DateTime, onupdate=func.now(), nullable=True)
    is_visible = Column(Boolean, default=True)
