import datetime
from typing import Optional

from pydantic import BaseModel


class BlogPostBase(BaseModel):
    title: str
    text: str
    is_visible: Optional[bool]


class BlogPostUpdate(BaseModel):
    title: str
    text: str
    is_visible: bool


class BlogPostCreate(BlogPostBase):
    pass


class BlogPost(BlogPostBase):
    id: int
    date_added: datetime.datetime
    date_updated: Optional[datetime.datetime]

    class Config:
        orm_mode = True
