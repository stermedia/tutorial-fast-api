import os

import pytest as pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from starlette.testclient import TestClient

from app.api import models
from app.config import Settings, get_settings
from app.database import get_db
from app.main import app


def override_get_settings() -> Settings:
    return Settings(app_name="Welcome from unittests",
                    db_name=os.environ.get("DATABASE_NAME_TEST"))


settings = override_get_settings()
SQLALCHEMY_TEST_DATABASE_URL = f"postgresql://{settings.db_user}:{settings.db_password}@{settings.db_host}/{settings.db_name}"

engine = create_engine(SQLALCHEMY_TEST_DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

models.Base.metadata.create_all(bind=engine)


def override_get_db() -> None:
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


@pytest.fixture(scope="function")
def test_client():
    app.dependency_overrides[get_settings] = override_get_settings
    app.dependency_overrides[get_db] = override_get_db
    client = TestClient(app)

    yield client

    db = TestingSessionLocal()
    db.query(models.BlogPost).delete()
    db.commit()
