def test_origin_endpoint(test_client):
    res = test_client.get('/origin/')

    data = res.json()
    assert res.status_code == 200
    assert data.get('app_name') == 'Welcome from unittests'
