import json


def test_create_blog_post_successfully(test_client):
    data = {
        'title': 'Blog post title.',
        'text': 'This is my first blog post.',
        'is_visible': True,
    }
    res = test_client.post('/posts/', data=json.dumps(data))

    res_data = res.json()
    assert res.status_code == 201
    assert res_data.get('title') == 'Blog post title.'
    assert res_data.get('text') == 'This is my first blog post.'
    assert res_data.get('date_added')
    assert not res_data.get('date_updated')
    assert res_data.get('is_visible')


def test_create_blog_post_without_required_parameters(test_client):
    data = {
        'is_visible': True,
    }
    res = test_client.post('/posts/', data=json.dumps(data))

    res_data = res.json()
    assert res.status_code == 422
    assert res_data == {'detail': [{'loc': ['body', 'title'], 'msg': 'field required', 'type': 'value_error.missing'},
                                   {'loc': ['body', 'text'], 'msg': 'field required', 'type': 'value_error.missing'}]}


def test_create_blog_post_with_incorrect_parameters(test_client):
    data = {
        'title': 'Blog post title.',
        'text': 'This is my first blog post.',
        'is_visible': 'aaa',
    }

    res = test_client.post('/posts/', data=json.dumps(data))

    res_data = res.json()
    assert res.status_code == 422
    assert res_data == {'detail': [
        {'loc': ['body', 'is_visible'], 'msg': 'value could not be parsed to a boolean', 'type': 'type_error.bool'}]}


def test_get_blog_post_with_given_id(test_client):
    data = {
        'title': 'Blog post title.',
        'text': 'This is my first blog post.',
        'is_visible': False,
    }
    res = test_client.post('/posts/', data=json.dumps(data))
    blog_post_id = res.json().get('id')

    res = test_client.get(f'/posts/{blog_post_id}')
    res_data = res.json()

    assert res.status_code == 200
    assert res_data.get('id') == blog_post_id
    assert res_data.get('title') == 'Blog post title.'
    assert res_data.get('text') == 'This is my first blog post.'
    assert res_data.get('date_added')
    assert not res_data.get('is_visible')
    assert not res_data.get('date_updated')


def test_get_blog_post_with_given_id_doesnt_exist(test_client):
    res = test_client.get(f'/posts/123456789')

    assert res.status_code == 404
    assert res.json().get('detail') == 'Blog post with given id does not exist.'


def test_get_blog_post_invalid_parameter(test_client):
    res = test_client.get(f'/posts/abcd')

    assert res.status_code == 422
    assert res.json() == {"detail": [
        {"loc": ["path", "blog_post_id"], "msg": "value is not a valid integer", "type": "type_error.integer"}]}


def test_list_blog_posts_successfully(test_client):
    res = test_client.post('/posts/', data=json.dumps(
        {'title': 'Blog post title.', 'text': 'This is my first blog post.', 'is_visible': False}))
    res = test_client.post('/posts/', data=json.dumps(
        {'title': 'Second blog post', 'text': 'This is my second blog post.', 'is_visible': True}))

    res = test_client.get(f'/posts/')
    res_data = res.json()

    assert res.status_code == 200
    assert res_data[0].get('title') == 'Blog post title.'
    assert res_data[0].get('text') == 'This is my first blog post.'
    assert res_data[0].get('date_added')
    assert not res_data[0].get('is_visible')
    assert not res_data[0].get('date_updated')

    assert res_data[1].get('title') == 'Second blog post'
    assert res_data[1].get('text') == 'This is my second blog post.'
    assert res_data[1].get('date_added')
    assert res_data[1].get('is_visible')
    assert not res_data[1].get('date_updated')

    assert len(res_data) == 2


def test_list_blog_posts_no_posts(test_client):
    res = test_client.get(f'/posts/')
    res_data = res.json()

    assert res.status_code == 200
    assert len(res_data) == 0


def test_update_blog_post_successfully(test_client):
    res = test_client.post('/posts/', data=json.dumps(
        {'title': 'Blog post title.', 'text': 'This is my first blog post.', 'is_visible': False}))
    blog_post_id = res.json().get('id')

    res = test_client.put(f'/posts/{blog_post_id}', data=json.dumps(
        {'title': 'Blog post new title.', 'text': 'A little bit changed text', 'is_visible': True}
    ))

    res_data = res.json()

    assert res.status_code == 200
    assert res_data.get('title') == 'Blog post new title.'
    assert res_data.get('text') == 'A little bit changed text'
    assert res_data.get('is_visible')
    assert res_data.get('date_added')
    assert res_data.get('date_updated')


def test_partially_update_blog_post_not_allowed(test_client):
    res = test_client.post('/posts/', data=json.dumps(
        {'title': 'Blog post title.', 'text': 'This is my first blog post.', 'is_visible': False}))
    blog_post_id = res.json().get('id')

    res = test_client.put(f'/posts/{blog_post_id}', data=json.dumps(
        {'title': 'Blog post new title.'}
    ))

    res_data = res.json()

    assert res.status_code == 422
    assert res_data == {'detail': [{'loc': ['body', 'text'], 'msg': 'field required', 'type': 'value_error.missing'},
                                   {'loc': ['body', 'is_visible'], 'msg': 'field required',
                                    'type': 'value_error.missing'}]}


def test_update_blog_post_id_doesnt_exist(test_client):
    res = test_client.put(f'/posts/123456789', data=json.dumps(
        {'title': 'Blog post new title.', 'text': 'A little bit changed text', 'is_visible': True}
    ))
    res_data = res.json()

    assert res.status_code == 404
    assert res_data.get('detail') == 'Blog post has not been updated.'


def test_delete_blog_post_successfully(test_client):
    res = test_client.post('/posts/', data=json.dumps(
        {'title': 'Blog post title.', 'text': 'This is my first blog post.', 'is_visible': False}))
    blog_post_id = res.json().get('id')

    res = test_client.delete(f'/posts/{blog_post_id}')
    res_data = res.json()

    assert res.status_code == 200
    assert res_data.get('detail') == 'Blog post has been deleted.'


def test_delete_blog_post_with_given_id_doesnt_exist(test_client):
    res = test_client.delete(f'/posts/123456')
    res_data = res.json()

    assert res.status_code == 404
    assert res_data.get('detail') == 'Blog post has not been deleted.'
