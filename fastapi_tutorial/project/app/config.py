import logging
import os
from functools import lru_cache

from pydantic import BaseSettings

log = logging.getLogger("uvicorn")


class Settings(BaseSettings):
    app_name: str = "Awesome Blog API"
    admin_email: str = "admin@example.com"
    db_user: str = os.environ.get("POSTGRES_USER", "postgres")
    db_password: str = os.environ.get("POSTGRES_PASSWORD", "postgres")
    db_name: str = os.environ.get("DATABASE_NAME_DEV", "db_development")
    db_host: str = os.environ.get("SQL_HOST", "db")


@lru_cache()
def get_settings() -> Settings:
    log.info("Loading config settings...")
    return Settings()
