from fastapi import FastAPI, Depends

from app.api import models, endpoints
from app.config import Settings, get_settings

from app.database import engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.include_router(endpoints.router)


@app.get('/origin/')
async def origin(settings: Settings = Depends(get_settings)):
    return {
        'app_name': settings.app_name,
        'admin_email': settings.admin_email,
    }
